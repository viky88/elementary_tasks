package com.company.number;


import com.company.Validator;
import com.company.annotations.DescriptionApp;
import com.company.enums.Message;
import com.company.enums.Symbol;
import com.company.number.constants.Range;

import java.lang.annotation.Annotation;
import java.util.Scanner;

public class NumberStringDemo {

    private static final Scanner scanner = new Scanner(System.in);
    private static String argument;

    public static void main(String[] args) {

        String res = "";
        int number;

        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);

        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());
        do {
            do {
                System.out.println(Message.ENTER_NUMBER.getMessage());
                argument = scanner.next();
            } while (!Validator.isOneNumericInArr(argument, annotation));

            if (Validator.isOneNumericInArr(argument, annotation)) {
                number = Integer.parseInt(argument);

                if (number > 0) {
                    if (argument.length() == 1) {
                        res = Units.getUnitByString(number);
                    } else if (argument.length() == 2) {
                        res = Decades.getDecadesByString(number);
                    } else if (argument.length() == 3) {
                        res = Hundreds.getHundredsByString(number);
                    } else if (argument.length() == 4) {
                        res = UnitThousands.getUnitThousandsByString(number);
                    } else if (argument.length() == 5) {
                        res = DecadesThousands.getDecadesThousandsByString(number);
                    } else if (argument.length() == 6) {
                        res = HundredsThousands.getHundredsThousandsByString(number);
                    } else {
                        System.out.println(Message.LARGE_NUMBER.getMessage());
                        continue;
                    }
                    System.out.println(argument + ": " + res.substring(0, 1).toUpperCase() + res.substring(1));
                }
            }
            System.out.println(Message.CONTINUE_APP.getMessage());
            argument = scanner.next();
        } while (Validator.continueApp(argument));
    }
}
