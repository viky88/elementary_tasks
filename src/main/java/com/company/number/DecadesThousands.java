package com.company.number;


import com.company.number.constants.Range;
import com.company.number.constants.UnitsEnum;

public interface DecadesThousands {

    static String getDecadesThousandsByString(int num) {
        String res = "";
        if (num % 1000 == 0) {
            return getDecadesThousandsDivByTenThousands(num);
        } else {
            int hundreds = num % 1000;
            if (hundreds >= 100) {
                res = getDecadesThousandsDivByTenThousands(num) + " " + Hundreds.getHundredsByString(hundreds);
            } else if (hundreds >= 10 && hundreds < 100) {
                res = getDecadesThousandsDivByTenThousands(num) + " " + Decades.getDecadesByString(hundreds);
            } else if (hundreds < 10) {
                res = getDecadesThousandsDivByTenThousands(num) + " " + Units.getUnitByString(hundreds);
            }
            return res;
        }
    }

    static String getDecadesThousandsDivByTenThousands(int num) {
        String res = "";
        int decades = num / 1000;
        int decadesDivByTen = decades / 10 * 10;
        if (decades >= 20) {
            if (decades % 10 == 1) {
                res = Decades.getDecadesDivByTen(decadesDivByTen) + " " + UnitsEnum.ONEf.getUnits() + " " +
                        Range.THOUSANDS.getRange() + "а";
            } else if (decades % 10 == 2) {
                res = Decades.getDecadesDivByTen(decadesDivByTen) + " " + UnitsEnum.TWOf.getUnits() + " " +
                        Range.THOUSANDS.getRange() + "и";
            } else if (decades % 10 == 3 || decades % 10 == 4) {
                res = Decades.getDecadesByString(decades) + " " + Range.THOUSANDS.getRange() + "и";
            } else {
                res = Decades.getDecadesByString(decades) + " " + Range.THOUSANDS.getRange();
            }
        } else {
            res = Decades.getDecadesByString(decades) + " " + Range.THOUSANDS.getRange();
        }
        return res;
    }
}
