package com.company.number;


import com.company.number.constants.Range;
import com.company.number.constants.UnitsEnum;

public interface UnitThousands {

    static String getUnitThousandsByString(int num) {
        if (num % 1000 == 0) {
            return getUnitThousandsDivByThousands(num);
        } else {
            int thousands = num / 1000 * 1000;
            int hundreds = num % 1000;
            String res = "";
            if (hundreds >= 100) {
                res = getUnitThousandsDivByThousands(thousands) + " " + Hundreds.getHundredsByString(hundreds);
            } else if (hundreds >= 10 && hundreds < 100) {
                res = getUnitThousandsDivByThousands(thousands) + " " + Decades.getDecadesByString(hundreds);
            } else if (hundreds < 10) {
                res = getUnitThousandsDivByThousands(thousands) + " " + Units.getUnitByString(hundreds);
            }
            return res;
        }
    }

    static String getUnitThousandsDivByThousands(int num) {
        int unit = num / 1000;
        switch (num) {
            case 1000:
                return UnitsEnum.ONEf.getUnits() + " " + Range.THOUSANDS.getRange() + "а";
            case 2000:
                return UnitsEnum.TWOf.getUnits() + " " + Range.THOUSANDS.getRange() + "и";
            case 3000:
            case 4000:
                return Units.getUnitByString(unit) + " " + Range.THOUSANDS.getRange() + "и";
            default:
                return Units.getUnitByString(unit) + " " + Range.THOUSANDS.getRange();
        }
    }
}
