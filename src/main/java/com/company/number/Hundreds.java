package com.company.number;


import com.company.number.constants.HundredsEnum;

public interface Hundreds {

    static String getHundredsByString(int num) {
        if (num % 100 == 0) {
            return getHundredsDivByOneHundred(num);
        } else {
            int hund = num / 100 * 100;
            String res = "";
            if (num % 100 >= 10) {
                int dec = num % 100;
                res = getHundredsDivByOneHundred(hund) + " ";
                res += Decades.getDecadesByString(dec);
            } else if (num % 100 < 10){
                int unit = num % 100;
                res = getHundredsDivByOneHundred(hund) + " ";
                res += Units.getUnitByString(unit);
            }
            return res;
        }
    }

    static String getHundredsDivByOneHundred(int num) {
        switch (num) {
            case 100:
                return HundredsEnum.ONE_HUNDRED.getHundred();
            case 200:
                return HundredsEnum.TWO_HUNDRED.getHundred();
            case 300:
                return HundredsEnum.THREE_HUNDRED.getHundred();
            case 400:
                return HundredsEnum.FOUR_HUNDRED.getHundred();
            case 500:
                return HundredsEnum.FIVE_HUNDRED.getHundred();
            case 600:
                return HundredsEnum.SIX_HUNDRED.getHundred();
            case 700:
                return HundredsEnum.SEVEN_HUNDRED.getHundred();
            case 800:
                return HundredsEnum.EIGHT_HUNDRED.getHundred();
            case 900:
                return HundredsEnum.NINE_HUNDRED.getHundred();
            default:
                return null;
        }
    }
}
