package com.company.number;


import com.company.number.constants.UnitsEnum;

public interface Units {

    static String getUnitByString(int unit) {

        switch (unit) {
            case 0:
                return UnitsEnum.NULL.getUnits();
            case 1:
                return UnitsEnum.ONEm.getUnits();
            case 2:
                return UnitsEnum.TWOm.getUnits();
            case 3:
                return UnitsEnum.THREE.getUnits();
            case 4:
                return UnitsEnum.FOUR.getUnits();
            case 5:
                return UnitsEnum.FIVE.getUnits();
            case 6:
                return UnitsEnum.SIX.getUnits();
            case 7:
                return UnitsEnum.SEVEN.getUnits();
            case 8:
                return UnitsEnum.EIGHT.getUnits();
            case 9:
                return UnitsEnum.NINE.getUnits();
            default:
                System.out.println(UnitsEnum.values());
        }
        return null;
    }
}
