package com.company.number.constants;


public enum UnitsEnum {
    NULL("ноль"),
    ONEm("один"),
    ONEf("одна"),
    TWOm("два"),
    TWOf("две"),
    THREE("три"),
    FOUR("четыре"),
    FIVE("пять"),
    SIX("шесть"),
    SEVEN("семь"),
    EIGHT("восемь"),
    NINE("девять");

    private String units;

    UnitsEnum(String units) {
        this.units = units;
    }

    public String getUnits() {
        return units;
    }
}
