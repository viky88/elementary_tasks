package com.company.number.constants;


import com.company.annotations.DescriptionApp;

@DescriptionApp(description = "This application returns string of number what you enter.\n" +
        "Argument is one number can be max 999999.\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public enum Range {
    UNITS("единицы"),
    DECADES("десятки"),
    HUNDREDS("сотни"),
    THOUSANDS("тысяч"),
    MILLIONS("миллион");

    private String range;

    Range(String range) {
        this.range = range;
    }

    public String getRange() {
        return range;
    }
}
