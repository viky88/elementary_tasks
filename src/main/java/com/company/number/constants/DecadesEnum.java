package com.company.number.constants;


public enum DecadesEnum {
    TEN("десять"),
    ELEVEN("одиннадцать"),
    TWELVE("двенадцать"),
    THIRTEEN("тринадцать"),
    FOURTEEN("четырнадцать"),
    FIFTEEN("пятнадцать"),
    SIXTEEN("шестнадцать"),
    SEVENTEEN("семнадцать"),
    EIGHTEEN("восемнадцать"),
    NINETEEN("девятнадцать"),
    TWENTY("двадцать"),
    THIRTY("тридцать"),
    FORTY("сорок"),
    FIFTY("пятьдесят"),
    SIXTY("шестьдесят"),
    SEVENTY("семьдесят"),
    EIGHTY("восемьдесят"),
    NINETY("девяносто");

    private String decadec;

    DecadesEnum(String decadec) {
        this.decadec = decadec;
    }

    public String getDecadec() {
        return decadec;
    }
}
