package com.company.number.constants;


public enum HundredsEnum {
    ONE_HUNDRED("сто"),
    TWO_HUNDRED("двести"),
    THREE_HUNDRED("триста"),
    FOUR_HUNDRED("четыресто"),
    FIVE_HUNDRED("пятьсот"),
    SIX_HUNDRED("шестьсот"),
    SEVEN_HUNDRED("семьсот"),
    EIGHT_HUNDRED("восемьсот"),
    NINE_HUNDRED("девятьсот");

    private String hundred;

    HundredsEnum(String hundred) {
        this.hundred = hundred;
    }

    public String getHundred() {
        return hundred;
    }
}
