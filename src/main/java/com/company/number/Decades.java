package com.company.number;


import com.company.number.constants.DecadesEnum;

public interface Decades {

    static String getDecadesByString(int decades) {
        if (decades != 10 && decades < 20) {
            return getFromElevenToNineteen(decades);
        } else if (decades % 10 == 0) {
            return getDecadesDivByTen(decades);
        } else {
            int num = decades / 10 * 10;
            int units = decades % 10;
            String dec = getDecadesDivByTen(num) + " ";
            dec += Units.getUnitByString(units);
            return dec;
        }
    }

    static String getFromElevenToNineteen(int num) {
        switch (num) {
            case 11:
                return DecadesEnum.ELEVEN.getDecadec();
            case 12:
                return DecadesEnum.TWELVE.getDecadec();
            case 13:
                return DecadesEnum.THIRTEEN.getDecadec();
            case 14:
                return DecadesEnum.FOURTEEN.getDecadec();
            case 15:
                return DecadesEnum.FIFTEEN.getDecadec();
            case 16:
                return DecadesEnum.SIXTEEN.getDecadec();
            case 17:
                return DecadesEnum.SEVENTEEN.getDecadec();
            case 18:
                return DecadesEnum.EIGHTEEN.getDecadec();
            case 19:
                return DecadesEnum.NINETEEN.getDecadec();
            default:
                return null;
        }
    }

    static String getDecadesDivByTen(int num) {
        switch (num) {
            case 10:
                return DecadesEnum.TEN.getDecadec();
            case 20:
                return DecadesEnum.TWENTY.getDecadec();
            case 30:
                return DecadesEnum.THIRTY.getDecadec();
            case 40:
                return DecadesEnum.FORTY.getDecadec();
            case 50:
                return DecadesEnum.FIFTY.getDecadec();
            case 60:
                return DecadesEnum.SIXTY.getDecadec();
            case 70:
                return DecadesEnum.SEVENTY.getDecadec();
            case 80:
                return DecadesEnum.EIGHTY.getDecadec();
            case 90:
                return DecadesEnum.NINETY.getDecadec();
            default:
                return null;
        }
    }
}
