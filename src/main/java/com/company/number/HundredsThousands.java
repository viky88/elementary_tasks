package com.company.number;


import com.company.number.constants.Range;

public interface HundredsThousands {

    static String getHundredsThousandsByString(int num) {
        int delim = num % 1000;
        if (delim == 0) {
            return hundredsThousands(num);
        } else {
            String res = "";
            int thousands = num / 1000 * 1000;
            if (delim < 1000 && delim >= 100) {
                res = hundredsThousands(thousands) + " " + Hundreds.getHundredsByString(delim);
            } else if (delim < 100 && delim >= 10) {
                res = hundredsThousands(thousands) + " " + Decades.getDecadesByString(delim);
            } else if (delim < 10) {
                res = hundredsThousands(thousands) + " " + Units.getUnitByString(delim);
            }
            return res;
        }
    }

    static String hundredsThousands(int num) {
        int houndreds = num / 1000;
        int thousands = num % 1000;
        int decadesThousands = num % 100000;
        int houndredsDivByHoundr = houndreds / 100 * 100;

        if (num % 100000 == 0) {
            return Hundreds.getHundredsByString(houndreds) + " " + Range.THOUSANDS.getRange();
        } else if (thousands == 0) {
            if (decadesThousands < 10000) {
                int unitsThousands = num % 10000;
                return Hundreds.getHundredsByString(houndredsDivByHoundr) + " " +
                        UnitThousands.getUnitThousandsByString(unitsThousands);
            } else if (decadesThousands >= 10000) {
                return Hundreds.getHundredsByString(houndredsDivByHoundr) + " " +
                        DecadesThousands.getDecadesThousandsByString(decadesThousands);
            }
        }
        return null;
    }
}
