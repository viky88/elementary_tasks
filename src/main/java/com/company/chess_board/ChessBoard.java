package com.company.chess_board;

import com.company.annotations.DescriptionApp;

@DescriptionApp(description = "This application returns image as a chessboard on the entered parameters at program " +
        "startup.\nYou need enter two numbers (height and width) each with new line for your chess board.\n" +
        "Example with height and width 4*4 \n\n" +
        "* * * * \n" +
        " * * * *\n" +
        "* * * * \n" +
        " * * * *\n\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class ChessBoard {

    private final String space = " ";
    private final String asterisk = "*";
    private int height;
    private int width;

    public ChessBoard(String height, String width) {
        this.height = Integer.parseInt(height);
        this.width = Integer.parseInt(width);
    }

    public void printBoard() {
        for (int i = 0; i <= height; i++) {
            for (int j = 0; j <= width; j++) {
                if (i % 2 == 0) {
                    System.out.print(asterisk);
                    System.out.print(space);
                } else {
                    System.out.print(space);
                    System.out.print(asterisk);
                }
            }
            System.out.println();
        }
    }
}
