package com.company.chess_board;

import com.company.annotations.DescriptionApp;

import java.lang.annotation.Annotation;

import com.company.Validator;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.util.Scanner;


public class ChessBoardDemo {

    private static final Scanner scanner = new Scanner(System.in);
    private static String heightString;
    private static String widthString;
    private static String param;
    private static ChessBoard chessBoard;

    public static void main(String[] args) {

        Class<ChessBoard> chessBoardClass = ChessBoard.class;
        Annotation annotation = chessBoardClass.getDeclaredAnnotation(DescriptionApp.class);

        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());
        do {
            do {
                System.out.println(Message.ENTER_HEIGHT_BOARD.getMessage());
                heightString = scanner.next();
            } while (!Validator.isOneNumericInArr(heightString, annotation));

            do {
                System.out.println(Message.ENTER_WIDTH_BOARD.getMessage());
                widthString = scanner.next();
            } while (!Validator.isOneNumericInArr(widthString, annotation));

            chessBoard = new ChessBoard(heightString, widthString);
            chessBoard.printBoard();

            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();
        } while (Validator.continueApp(param));
    }
}
