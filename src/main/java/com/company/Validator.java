package com.company;


import com.company.annotations.DescriptionApp;
import com.company.enums.Message;
import com.company.fibonacci.Fibonacci;

import java.lang.annotation.Annotation;

public interface Validator {

    static boolean isOneNumericInArr(String param, Annotation annotation) {

        String description = ((DescriptionApp) annotation).description();

        if (param.isEmpty()) {
            System.out.println(description);
            return false;
        } else if (isNumeric(param)) {
            return true;
        } else {
            System.out.println(Message.INCORRECT_DATA.getMessage());
            return false;
        }
    }

    static boolean validatorForFibonacci(String param) {
        Class<Fibonacci> fibonacciClass = Fibonacci.class;
        Annotation annotation = fibonacciClass.getDeclaredAnnotation(DescriptionApp.class);
        String description = ((DescriptionApp) annotation).description();

        if (param.isEmpty()) {
            System.out.println(description);
            return false;
        } else if (isFibonacciArgs(param)) {
            return true;
        } else {
            System.out.println(Message.INCORRECT_DATA.getMessage());
            return false;
        }
    }

    static boolean isFibonacciArgs (String param) {
        if (param.matches("\\d+\\s+\\d+")) {
            return true;
        }
        return false;
    }

    static boolean isNumeric(String argument) {
        if (argument.matches("\\d+")) {
            return true;
        }
        return false;
    }

    static boolean continueApp(String param) {
        if (param.equalsIgnoreCase(Message.CONTINUE_Y.getMessage()) ||
                param.equalsIgnoreCase(Message.CONTINUE_YES.getMessage())) {
            return true;
        }
        return false;
    }

}
