package com.company.envelopes;


import java.util.ArrayList;
import java.util.List;


public class EnvelopeAnalysis {

    private static List<Envelope> envelopes = new ArrayList<>();

    public static void putEnvelope(Envelope envelope) {
        envelopes.add(envelope);
    }

    public static void clearAnalysis() {
        envelopes.clear();
    }

    public static boolean analysis() {
        Envelope firstEnvelope = envelopes.get(0);
        Envelope secondEnvelope = envelopes.get(1);
        double firstHeight = firstEnvelope.getHeight();
        double firstWidth = firstEnvelope.getWidth();
        double secondHeight = secondEnvelope.getHeight();
        double secondWidth = secondEnvelope.getWidth();

        if ((firstHeight > secondHeight) && (firstWidth > secondWidth) ||
                (firstHeight > secondWidth) && (firstWidth > secondHeight)) {
            return true;
        } else if ((secondHeight > firstHeight) && (secondWidth > firstWidth) ||
                (secondHeight > firstWidth) && (secondWidth > firstHeight)) {
            return true;
        } else {
            return false;
        }
    }
}
