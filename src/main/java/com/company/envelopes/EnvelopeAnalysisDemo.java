package com.company.envelopes;


import com.company.Validator;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.util.Scanner;

public class EnvelopeAnalysisDemo {

    private static final Scanner scanner = new Scanner(System.in);
    private static final int QUANTITY_ENVELOPE = 2;
    private static String height;
    private static String width;
    private static String param;

    public static void main(String[] args) {

        Envelope envelope;
        final String side1 = "height";
        final String side2 = "width";
        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());

        do {
            EnvelopeAnalysis.clearAnalysis();

            for (int i = 1; i <= QUANTITY_ENVELOPE; i++) {
                height = enterData(side1, i);
                width = enterData(side2, i);
                envelope = new Envelope(height, width);
                EnvelopeAnalysis.putEnvelope(envelope);
            }

            boolean nested = EnvelopeAnalysis.analysis();
            if (nested) {
                System.out.println(Message.ENVELOPE_RESULT_TRUE.getMessage());
            } else {
                System.out.println(Message.ENVELOPE_RESULT_FALSE.getMessage());
            }

            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();
        } while (Validator.continueApp(param));
    }

    public static String enterData(String nameSide, int numEnvelop) {
        String side;
        do {
            System.out.println("Enter " + nameSide + " your " + numEnvelop + " envelope:");
            side = scanner.next();
        } while (!ValidatorEnvelopes.validData(side));
        return side;
    }
}
