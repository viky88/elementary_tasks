package com.company.envelopes;

import com.company.annotations.DescriptionApp;

@DescriptionApp(description = "This program analyzes the possibility to enclose one envelope in another.\n" +
        "You need enter height and width each envelopes (each parameters enter in new line). Format parameters are" +
        " floating-point number. For example: 4.0\n" +
        "When you get result, you can continue envelope analysis. For that you need enter word \"yes\" or \"y\".\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class Envelope {

    private double height;
    private double width;

    public Envelope(String height, String width) {
        this.height = Double.parseDouble(height);
        this.width = Double.parseDouble(width);
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }
}
