package com.company.envelopes;


import com.company.annotations.DescriptionApp;
import com.company.enums.Message;

import java.lang.annotation.Annotation;

public class ValidatorEnvelopes {

    static boolean validData(String param) {
        Class<Envelope> envelopeClass = Envelope.class;
        Annotation annotation = envelopeClass.getDeclaredAnnotation(DescriptionApp.class);
        String description = ((DescriptionApp) annotation).description();

        if (param.isEmpty()) {
            System.out.println(description);
            return false;
        } else if (isParamEnvelop(param)) {
            return true;
        } else {
            System.out.println(Message.INCORRECT_DATA);
            return false;
        }
    }

    private static boolean isParamEnvelop(String param) {
        if (param.matches("\\d+\\.\\d{1,2}")) {
            return true;
        }
        return false;
    }
}
