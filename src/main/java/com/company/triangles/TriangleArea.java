package com.company.triangles;


public class TriangleArea {

    private static double area;

    protected static double countArea(double side1, double side2, double side3) {
        double semiperimeter;

        semiperimeter = (side1 + side2 + side3) / 2;
        area = Math.sqrt(semiperimeter * (semiperimeter - side1) * (semiperimeter - side2) * (semiperimeter - side3));
        return area;
    }
}
