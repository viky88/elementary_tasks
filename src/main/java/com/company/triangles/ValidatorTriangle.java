package com.company.triangles;

import com.company.annotations.DescriptionApp;
import com.company.enums.Message;

import java.lang.annotation.Annotation;

public class ValidatorTriangle {

    private static final Class<Triangle> triangleClass = Triangle.class;
    private static final Annotation annotation = triangleClass.getDeclaredAnnotation(DescriptionApp.class);
    private static final String description = ((DescriptionApp) annotation).description();
    private static final String matches = "[^\\s]+\\s*,\\s*\\d{0,9}(\\.\\d{1,2})\\s*,\\s*\\d{0,9}(\\.\\d{1,2})" +
            "\\s*,\\s*\\d{0,9}(\\.\\d{1,2})";

    public static boolean isTriangle(double side1, double side2, double side3) {
        boolean isTriangle = false;
        if (((side1 + side2) > side3) && ((side2 + side3) > side1) && ((side1 + side3) > side2)) {
            isTriangle = true;
        }
        return isTriangle;
    }

    public static boolean validData(String param) {

        if (param.isEmpty()) {
            System.out.println(description);
            return false;
        } else if (isParamTriangle(param)) {
            return true;
        }
        System.out.println(Message.INCORRECT_DATA.getMessage());
        return false;
    }

    private static boolean isParamTriangle(String line) {
        boolean isParam = false;
        if (line.matches(matches)) {
            isParam = true;
        }
        return isParam;
    }

}
