package com.company.triangles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public interface TrianglesBox {

    List<Triangle> triangleList = new ArrayList<>();

    static void putTheBox(Triangle triangle) {
        triangleList.add(triangle);
    }

    static List<Triangle> sortStreamTriangle() {
        List<Triangle> result = triangleList.stream().sorted(Triangle.AreaComparator).collect(Collectors.toList());
        return result;
    }
}
