package com.company.triangles;

import com.company.annotations.DescriptionApp;

import java.util.Comparator;

@DescriptionApp(description = "This program return sorted triangle.\n" +
        "You need enter name triangle and parameters three sides. All parameters must be entered in one line\n" +
        " separated by commas. Format parameters are floating-point number.\n" +
        "For example: <Name>, <side1> , <side2>, <side3> -> Triangle1, 2.0, 2.0, 4.0\n" +
        "When you enter triangle, you can continue enter another triangle. For that you need enter word \"yes\" or" +
        " \"y\".\nAfter that, if you don't press continue, you will get sorted list triangle for area and program" +
        " will be stop\nIf you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class Triangle {

    private String name;
    private double lengthOfSize1;
    private double lengthOfSize2;
    private double lengthOfSize3;
    private double area;

    public Triangle(String... args) {
        this.name = args[0];
        this.lengthOfSize1 = Double.parseDouble(args[1]);
        this.lengthOfSize2 = Double.parseDouble(args[2]);
        this.lengthOfSize3 = Double.parseDouble(args[3]);
    }

    public double getLengthOfSize1() {
        return lengthOfSize1;
    }

    public double getLengthOfSize2() {
        return lengthOfSize2;
    }

    public double getLengthOfSize3() {
        return lengthOfSize3;
    }

    public double getArea() {
        area = TriangleArea.countArea(lengthOfSize1, lengthOfSize2, lengthOfSize3);
        return area;
    }

    @Override
    public String toString() {
        return "[Triangle " + name + "]: " + getArea() + " cm";
    }

    public static Comparator<Triangle> AreaComparator = (o1, o2) -> {
        Double areaTriangle1 = o1.getArea();
        Double areaTriangle2 = o2.getArea();
        int res = areaTriangle1.compareTo(areaTriangle2);
        return res;
    };
}
