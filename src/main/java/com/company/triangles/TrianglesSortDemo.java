package com.company.triangles;

import com.company.Validator;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.util.List;
import java.util.Scanner;

public class TrianglesSortDemo {

    private static final Scanner scanner = new Scanner(System.in);
    private static String param;

    public static void main(String[] args) {

        Triangle triangle;

        do {
            do {
                System.out.println(Message.ENTER_DATA_TRIANGLE.getMessage());
                scanner.useDelimiter(Symbol.DELIMETER.getSymbol());
                param = scanner.next();
            } while (!ValidatorTriangle.validData(param));

            String[] parameters = param.trim().split(Symbol.SPLITERATOR_COMMA.getSymbol());
            triangle = new Triangle(parameters);
            if (ValidatorTriangle.isTriangle(triangle.getLengthOfSize1(), triangle.getLengthOfSize2(),
                    triangle.getLengthOfSize3())) {
                TrianglesBox.putTheBox(triangle);
            } else {
                System.out.println(Message.NOT_TRIANGLE.getMessage());
            }

            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();

        } while (Validator.continueApp(param));

        List<Triangle> sortedListTriangle = TrianglesBox.sortStreamTriangle();
        for (Triangle tr : sortedListTriangle) {
            System.out.println(tr);
        }

    }
}
