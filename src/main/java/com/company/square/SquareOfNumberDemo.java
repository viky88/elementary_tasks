package com.company.square;


import com.company.Validator;
import com.company.annotations.DescriptionApp;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.lang.annotation.Annotation;
import java.util.Scanner;


public class SquareOfNumberDemo {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int number;
        Class<SquareOfNumber> squareOfNumberClass = SquareOfNumber.class;
        Annotation annotation = squareOfNumberClass.getDeclaredAnnotation(DescriptionApp.class);
        String param;
        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());

        do {
            do {
                System.out.println(Message.ENTER_NUMBER.getMessage());
                param = scanner.next();
            } while (!Validator.isOneNumericInArr(param, annotation));

            number = Integer.parseInt(param);
            SquareOfNumber squareOfNumber = new SquareOfNumber(number);
            squareOfNumber.getNumbersList();
            squareOfNumber.printList();

            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();
        }  while (Validator.continueApp(param));
    }
}
