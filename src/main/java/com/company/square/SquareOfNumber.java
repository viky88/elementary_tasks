package com.company.square;


import com.company.annotations.DescriptionApp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@DescriptionApp(description = "This application returns list of the natural numbers of whose square is not higher " +
        "than the value that you enter.\n" +
        "Argument can be any one number.\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class SquareOfNumber {

    List<Integer> numbersList = new ArrayList<>();
    int enterNumber;
    private final int SQUARE = 2;

    public SquareOfNumber(int enterNumber) {
        this.enterNumber = enterNumber;
    }

    public List<Integer> getNumbersList() {
        int num = 0;
        while (true) {
            num++;
            int squareNum = (int) Math.pow(num, SQUARE);
            if (squareNum < enterNumber) {
                numbersList.add(num);
            } else {
                return numbersList;
            }
        }
    }

    public void printList() {
        Iterator<Integer> iterator = numbersList.iterator();
        if (iterator.hasNext()) {
            System.out.print(iterator.next());
            while (iterator.hasNext()) {
                System.out.print(", " + iterator.next());
            }
        }
    }
}
