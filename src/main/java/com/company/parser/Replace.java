package com.company.parser;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Replace {

    String search;
    String replace;
    String newContent;

    public Replace(String search, String replace) {
        this.search = search;
        this.replace = replace;
    }

    public String getReplace(String fileRead) {
        newContent = fileRead.replaceAll(search, replace);
        return newContent;
    }

    public void writeFile(String pathName) {
        try {
            FileWriter fw = new FileWriter(pathName);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(newContent);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
