package com.company.parser;


import com.company.annotations.DescriptionApp;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.lang.annotation.Annotation;
import java.util.Scanner;

public class ParserDemo {

    public static void main(String[] args) {
//        String filePath = "/Users/viktoriyasidenko/Documents/Projects/SoftServeITAcademy/elementary_tasks/src/main/fileExample.txt";

        Scanner scanner = new Scanner(System.in);
        System.out.println("Select and enter number for modes:\n1 - Read the number of occurrences of a string in a " +
                "text file\n2 - Make a replacement of a string by another in the specified file");
        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());
        ScanInputData scanInputData;
        while (true) {
            if (scanner.hasNextInt()) {
                int modes = scanner.nextInt();
                if (modes <= 2) {
                    switch (modes) {
                        case 1:
                            scanInputData = new ScanInputData();
                            scanInputData.scanCount();
                            break;
                        case 2:
                            scanInputData = new ScanInputData();
                            scanInputData.scanChange();
                            break;
                        default:
                            return;
                    }
                    break;
                } else {
                    System.out.println("1 or 2");
                    continue;
                }
            } else if (scanner.hasNext("")) {
                Class<ReadFile> readFileClass = ReadFile.class;
                Annotation annotation = readFileClass.getDeclaredAnnotation(DescriptionApp.class);
                String description = ((DescriptionApp) annotation).description();
                System.out.println(description);
            } else {
                System.out.println(Message.INCORRECT_DATA.getMessage());
            }
            scanner.next();
        }
    }
}

