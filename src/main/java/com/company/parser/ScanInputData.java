package com.company.parser;


import java.io.File;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Scanner;

import com.company.annotations.DescriptionApp;


public class ScanInputData {

    String pathName;
    String line;
    String change;
    Scanner scanner = new Scanner(System.in);
    Class<ReadFile> readFileClass = ReadFile.class;
    Annotation annotation = readFileClass.getDeclaredAnnotation(DescriptionApp.class);
    String description = ((DescriptionApp) annotation).description();


    public void scanCount() {
        scanner.useDelimiter("\n");
        System.out.println("Enter the path to the file and word you want to count");
        while (true) {
            if (scanner.hasNext("[^\\s]+\\s[^\\s]+")) {
                scanner.useDelimiter("\n|\\s");
                pathName = scanner.next();
                line = scanner.next();
                if (isFileExists(pathName)) {
                    ReadFile rf = new ReadFile(pathName);
                    rf.read();
                    List<String> words = rf.getListWords();
                    CountString countString = new CountString(words, line);
                    int iterated = countString.getIteratedWords();
                    System.out.println("Number of iterations of words " + "\"" + line + "\"" + " is " + iterated);
                    break;
                } else {
                    System.out.println(pathName + " file not exist");
                    scanner.skip("\\s");
                    continue;
                }
            } else if (scanner.hasNext("")) {
                scanner.useDelimiter("\n");
                System.out.println(description);
                scanner.skip("\n");
            } else {
                scanner.useDelimiter("\n");
                System.out.println("Incorrect data");
            }
            scanner.nextLine();
        }

    }

    public void scanChange() {
        scanner.useDelimiter("\n");
        System.out.println("Enter the path to the file, word which you want to replace and word on which to change");
        while (true) {
            if (scanner.hasNext("[^\\s]+\\s[^\\s]+\\s[^\\s]+\\s")) {
                scanner.useDelimiter("\\s");
                pathName = scanner.next();
                line = scanner.next();
                change = scanner.next();
                if (isFileExists(pathName)) {
                    ReadFile rf = new ReadFile(pathName);
                    rf.read();
                    String readFile = rf.getLineRead();
                    Replace replace = new Replace(line, change);
                    replace.getReplace(readFile);
                    replace.writeFile(pathName);
                    System.out.println("Yor file " + pathName + " was changed");
                    break;
                } else {
                    System.out.println(pathName + " file not exist");
                    scanner.useDelimiter("\n");
                    scanner.skip("\\s");
                }
                continue;
            } else if (scanner.hasNext("")) {
                scanner.useDelimiter("\n");
                System.out.println(description);
                scanner.skip("\n");
            } else {
                scanner.useDelimiter("\n");
                System.out.println("Incorrect data");
            }
            scanner.nextLine();
        }

    }

    private boolean isFileExists(String path) {
        boolean validator = false;
        if ((new File(path)).exists()) {
            validator = true;
        }
        return validator;
    }
}
