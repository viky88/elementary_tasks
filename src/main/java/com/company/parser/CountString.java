package com.company.parser;


import java.util.ArrayList;
import java.util.List;

public class CountString {

    private List<String> list;
    private String word;

    public CountString(List<String> list, String word) {
        this.list = list;
        this.word = word;
    }

    public int getIteratedWords() {
        int count = 0;

        for (String s : list) {
            if (s.equals(word)) {
                count += 1;
            }
        }
        return count;
    }


}
