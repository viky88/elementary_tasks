package com.company.parser;


import com.company.annotations.DescriptionApp;

import java.io.*;
import java.util.Arrays;
import java.util.List;

@DescriptionApp(description = "There is program have two modes:\n1. Read the number of occurrences of a string in a " +
        "text file\n2. Make a replacement of a string by another in the specified file\nYou need to select 1 or 2 " +
        "what you want to do.\nThe program accept input arguments at startup:\n- For first modes: path to file and" +
        " word for counting (example.txt ago)\n- For second modes: path to file, word for search and word for " +
        "replacement (example.txt ago go)\nIf you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.)")
public class ReadFile {

    List<String> listWords;
    String filePath;
    private StringBuilder lineRead = new StringBuilder();

    public ReadFile(String filePath) {
        this.filePath = filePath;
    }

    public List<String> getListWords() {
        listWords = listWords(makeLineRead());
        return listWords;
    }

    public String getLineRead() {
        return new String(lineRead);
    }

    public void read() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lineRead.append(line).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String makeLineRead() {
        return lineRead.substring(0, lineRead.length());
    }

    private List<String> listWords(String s) {
        String replace = s.replace(".", "").replace("!", "").replace(
                "?", "").toLowerCase();
        String[] arr = replace.split(" ");
        List<String> list = Arrays.asList(arr);
        return list;
    }

}
