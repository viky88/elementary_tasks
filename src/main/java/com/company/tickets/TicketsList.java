package com.company.tickets;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TicketsList {

    private final int LENGTH = 6;
    private List<Ticket> tickets = new ArrayList<>();
    private String pathName;

    public TicketsList(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public void readFile(List<Ticket> ticketsList) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathName))) {
            readTickets(bufferedReader, ticketsList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Ticket> readTickets(BufferedReader bufferedReader, List<Ticket> ticketsList) throws IOException {
        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            if (isMyNumber(currentLine)) {
                int numberTicket = Integer.parseInt(currentLine);
                ticketsList.add(new Ticket(numberTicket));
            }
        }
        return ticketsList;
    }

    public int countLuckyTicketsMoscow(List<Ticket> tickets) {
        int count = 0;
        for (Ticket ticket : tickets) {
            if (LuckyTickets.isLuckyMoscow(ticket.getNumber())) {
                count++;
            }
        }
        return count;
    }

    public int countLuckyTicketsPiter(List<Ticket> tickets) {
        int count = 0;
        for (Ticket ticket : tickets) {
            if (LuckyTickets.isLuckyPiter(ticket.getNumber())) {
                count++;
            }
        }
        return count;
    }

    private boolean isMyNumber(String num) {
        int length = num.length();
        if (length == LENGTH) {
            return true;
        }
        return false;
    }
}
