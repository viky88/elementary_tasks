package com.company.tickets;


public class LuckyTickets {

    private static final int delim = 1000;
    private static final int length = 6;

    public static boolean isLuckyMoscow(int number) {
        int firstPart = number / delim;
        int secondPart = number % delim;
        int sumFirstPart = sumDigit(firstPart);
        int sumSecondPart = sumDigit(secondPart);

        if (sumFirstPart == sumSecondPart) {
            return true;
        }
        return false;
    }

    public static boolean isLuckyPiter(int number) {
        int sumEven = 0;
        int sumOdd = 0;
        for (int i = length; i > 0; i--) {
            int digit = number % 10;
            if (i % 2 == 0) {
                sumEven += digit;
            } else {
                sumOdd += digit;
            }
            number /= 10;
        }

        if (sumEven == sumOdd) {
            return true;
        }
        return false;
    }

    private static int sumDigit(int num) {
        int sum = 0;
        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }
        return sum;
    }
}
