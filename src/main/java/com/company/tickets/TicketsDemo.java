package com.company.tickets;


import com.company.Validator;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TicketsDemo {

    public static void main(String[] args) {

//        String filePath = "/Users/viktoriyasidenko/Documents/Projects/SoftServeITAcademy/elementary_tasks/src/main/tickets.txt";

        List<Ticket> tickets = new ArrayList<>();
        TicketsList ticketsList = new TicketsList(tickets);

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());
        String path;
        String choose;
        String param;
        int lucky = 0;

        do {
            do {
                System.out.println(Message.ENTER_PATH.getMessage());
                path = scanner.next();
            } while (!ValidatorTickets.validatorFileExist(path));

            ticketsList.setPathName(path);


            do {
                System.out.println(Message.CHOOSE_FOR_TICKETS.getMessage());
                choose = scanner.next();
            } while (!ValidatorTickets.validatorTickets(choose));

            ticketsList.readFile(tickets);
            if (choose.equalsIgnoreCase(Message.TICKETS_MOSCOW.getMessage())) {
                lucky = ticketsList.countLuckyTicketsMoscow(tickets);
            } else if (choose.equalsIgnoreCase(Message.TICKETS_PITER.getMessage())) {
                lucky = ticketsList.countLuckyTicketsPiter(tickets);
            }

            System.out.println(lucky);
            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();
        } while (Validator.continueApp(param));
    }
}
