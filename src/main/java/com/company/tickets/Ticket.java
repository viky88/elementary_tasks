package com.company.tickets;

import com.company.annotations.DescriptionApp;

@DescriptionApp(description = "This application returns quantity happy tickets from your .txt file to counted \n" +
        "different ways (like Moscow or Piter way). You need enter path name your file.\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class Ticket {

    private int number;

    public Ticket(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        return number == ticket.number;
    }

    @Override
    public int hashCode() {
        return number;
    }
}
