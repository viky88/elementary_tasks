package com.company.tickets;


import com.company.annotations.DescriptionApp;

import java.lang.annotation.Annotation;

import com.company.enums.Message;
import com.company.enums.Symbol;
import com.company.tickets.Ticket;

import java.io.File;

public interface ValidatorTickets {

    Class<Ticket> ticketClass = Ticket.class;
    Annotation annotation = ticketClass.getDeclaredAnnotation(DescriptionApp.class);
    String description = ((DescriptionApp) annotation).description();

    static boolean validatorTickets(String line) {

        if (line.isEmpty()) {
            System.out.println(description);
            return false;
        } else if ((line.equalsIgnoreCase(Message.TICKETS_MOSCOW.getMessage())) ||
                (line.equalsIgnoreCase(Message.TICKETS_PITER.getMessage()))) {
            return true;
        } else {
            System.out.println(Message.INCORRECT_DATA.getMessage() + Symbol.DELIMETER.getSymbol());
            return false;
        }
    }

    static boolean validatorFileExist(String path) {

        if (path.isEmpty()) {
            System.out.println(description);
            return false;
        } else if (new File(path).exists()) {
            return true;
        } else {
            System.out.println(Message.INCORRECT_DATA.getMessage() + Symbol.DELIMETER.getSymbol());
            return false;
        }
    }

}
