package com.company.enums;


public enum Symbol {

    DELIMETER("\n"),
    SPLITERATOR_COMMA(","),
    SPLITERATOR_SPACE(" ");

    String symbol;

    Symbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
