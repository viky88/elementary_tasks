package com.company.enums;


public enum Message {
    INCORRECT_DATA("Incorrect data"),
    ENTER_NUMBER("Enter your number"),
    ENTER_RANGE("Enter your range"),
    ENTER_PATH("Enter path name"),
    NOT_TRIANGLE("Your parameters aren't triangle"),
    CONTINUE_APP("\nIf you wont continue you will enter \"y\" or \"yes\""),
    CONTINUE_Y("y"),
    CONTINUE_YES("yes"),

    ENTER_DATA_TRIANGLE("Enter data your triangle"),

    ENTER_HEIGHT_BOARD("Enter height your chess board:"),
    ENTER_WIDTH_BOARD("Enter width your chess board:"),

    CHOOSE_FOR_TICKETS("Choose how to count the lucky tickets (enter Mockow or Piter):"),
    TICKETS_MOSCOW("moscow"),
    TICKETS_PITER("piter"),

    ENVELOPE_RESULT_TRUE("RESULT: Envelopes are nested"),
    ENVELOPE_RESULT_FALSE("RESULT: Envelopes aren't nested"),

    LARGE_NUMBER("Number is too large");



    String message;

    Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
