package com.company.fibonacci;


import com.company.Validator;
import com.company.enums.Message;
import com.company.enums.Symbol;

import java.util.Scanner;

public class FibonacciDemo {

    private static final Scanner scanner = new Scanner(System.in);
    private static String param;

    public static void main(String[] args) {

        scanner.useDelimiter(Symbol.DELIMETER.getSymbol());

        do {
            do {
                System.out.println(Message.ENTER_RANGE.getMessage());
                param = scanner.next();
            } while (!Validator.validatorForFibonacci(param));

            String[] arguments = param.split(Symbol.SPLITERATOR_SPACE.getSymbol());
            Fibonacci fibonacci = new Fibonacci(arguments);
            fibonacci.getNumbersFib();

            System.out.println(Message.CONTINUE_APP.getMessage());
            param = scanner.next();
        } while (Validator.continueApp(param));
    }
}
