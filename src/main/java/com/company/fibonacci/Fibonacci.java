package com.company.fibonacci;

import com.company.annotations.DescriptionApp;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

@DescriptionApp(description = "This application returns number Fibonacci in range what you enter. \n" +
        "Arguments are entered through a space.\n" +
        "If you don't enter parameters, you will get this message again.\n" +
        "If you enter not valid symbols, you will get massage about error.\n")
public class Fibonacci {

    private String[] args;
    private int start;
    private int end;
    private List<Integer> listFibonacci;

    public Fibonacci(String... args) {
        this.args = args;
        start = Integer.parseInt(args[0]);
        end = Integer.parseInt(args[1]);
    }

    public void getNumbersFib() {
        numberFib();
        for (Integer num : listFibonacci) {
            System.out.println(num);
        }
    }

    public List<Integer> numberFib() {
        listFibonacci = new ArrayList<>();
        int numOne = 0;
        int numTwo = 1;
        int num;
        while (true) {
            if (numTwo >= start) {
                listFibonacci.add(numTwo);
            }
            num = numTwo;
            numTwo += numOne;
            numOne = num;
            if (numTwo >= end) {
                return listFibonacci;
            }
        }
    }
}
