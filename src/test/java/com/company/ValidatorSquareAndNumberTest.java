package com.company;

import com.company.annotations.DescriptionApp;
import com.company.number.constants.Range;
import org.junit.Assert;
import org.junit.Test;

import java.lang.annotation.Annotation;

import static org.junit.Assert.*;


public class ValidatorSquareAndNumberTest {

    @Test
    public void isNumericForSquareArrLong() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "23 54";
        Assert.assertFalse(Validator.isOneNumericInArr(param, annotation));
    }

    @Test
    public void isNumericForSquareEmpty() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "";
        Assert.assertFalse(Validator.isOneNumericInArr(param, annotation));
    }

    @Test
    public void isNumericForSquareNotNumeric() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "wrongValue";
        Assert.assertFalse(Validator.isOneNumericInArr(param, annotation));
    }

    @Test
    public void isNumericForSquareDouble() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "6.9";
        Assert.assertFalse(Validator.isOneNumericInArr(param, annotation));
    }

    @Test
    public void isNumericForSquareNegative() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "-7";
        Assert.assertFalse(Validator.isOneNumericInArr(param, annotation));
    }

    @Test
    public void isNumericForSquareInteger() throws Exception {
        Class<Range> rangeClass = Range.class;
        Annotation annotation = rangeClass.getDeclaredAnnotation(DescriptionApp.class);
        String param = "54";
        Assert.assertTrue(Validator.isOneNumericInArr(param, annotation));
    }
}