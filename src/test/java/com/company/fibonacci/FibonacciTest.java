package com.company.fibonacci;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class FibonacciTest {

    private Fibonacci fibonacci;

    @Before
    public void setUp() {
        String[] range = {"10", "50"};
        fibonacci = new Fibonacci(range);
    }

    @Test
    public void numberFib() throws Exception {
        List<Integer> expected = new ArrayList<>();
        expected.add(13);
        expected.add(21);
        expected.add(34);

        List<Integer> fib = fibonacci.numberFib();

        Assert.assertEquals(expected, fib);
    }

}