package com.company;


import org.junit.Assert;
import org.junit.Test;

public class ValidatorFibonacciTest {

    String arg;

    @Test
    public void validatorForFibonacciEmpty() throws Exception {
        arg = "";
        Assert.assertFalse(Validator.validatorForFibonacci(arg));
    }

    @Test
    public void validatorForFibonacciArrLength1() throws Exception {
        arg = "34";
        Assert.assertFalse(Validator.validatorForFibonacci(arg));
    }

    @Test
    public void validatorForFibonacciArrLength3() throws Exception {
        arg = "34 453 long";
        Assert.assertFalse(Validator.validatorForFibonacci(arg));
    }

    @Test
    public void validatorForFibonacciNotNumber() throws Exception {
        arg = "not number";
        Assert.assertFalse(Validator.validatorForFibonacci(arg));
    }

    @Test
    public void validatorForFibonacciNegativeNumber() throws Exception {
        arg = "-5 -48";
        Assert.assertFalse(Validator.validatorForFibonacci(arg));
    }

    @Test
    public void validatorForFibonacciNumber() throws Exception {
        arg = "5 48";
        Assert.assertTrue(Validator.validatorForFibonacci(arg));
    }
}
