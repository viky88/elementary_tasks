package com.company.envelopes;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class EnvelopeAnalysisTest {

    @Test
    public void analysisDontNested() throws Exception {
        EnvelopeAnalysis.putEnvelope(new Envelope("3.5", "6.9"));
        EnvelopeAnalysis.putEnvelope(new Envelope("2.9", "7.5"));

        Assert.assertFalse(EnvelopeAnalysis.analysis());
    }

    @Test
    public void analysisFirstNestedSecondCondition1() {
        EnvelopeAnalysis.putEnvelope(new Envelope("5.9", "9.9"));
        EnvelopeAnalysis.putEnvelope(new Envelope("4.7", "8.5"));

        Assert.assertTrue(EnvelopeAnalysis.analysis());
    }

    @Test
    public void analysisFirstNestedSecondCondition2() {
        EnvelopeAnalysis.putEnvelope(new Envelope("5.9", "9.9"));
        EnvelopeAnalysis.putEnvelope(new Envelope("7.3", "4.4"));

        Assert.assertTrue(EnvelopeAnalysis.analysis());
    }

    @Test
    public void analysisSecondNestedFirstCondition1() {
        EnvelopeAnalysis.putEnvelope(new Envelope("7.0", "5.9"));
        EnvelopeAnalysis.putEnvelope(new Envelope("7.8", "6.9"));

        Assert.assertTrue(EnvelopeAnalysis.analysis());
    }

    @Test
    public void analysisSecondNestedFirstCondition2() {
        EnvelopeAnalysis.putEnvelope(new Envelope("5.8", "3.5"));
        EnvelopeAnalysis.putEnvelope(new Envelope("4.8", "6.9"));

        Assert.assertTrue(EnvelopeAnalysis.analysis());
    }

    @After
    public void clear() {
        EnvelopeAnalysis.clearAnalysis();
    }

}