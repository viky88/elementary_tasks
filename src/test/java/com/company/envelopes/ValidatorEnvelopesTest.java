package com.company.envelopes;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class ValidatorEnvelopesTest {

    String argument;

    @Test
    public void validData() throws Exception {
        argument = "2.9";
        Assert.assertTrue(ValidatorEnvelopes.validData(argument));
    }

    @Test
    public void validDataEmpty() throws Exception {
        argument = "";
        Assert.assertFalse(ValidatorEnvelopes.validData(argument));
    }

    @Test
    public void validDataAnyWord() throws Exception {
        argument = "Wrong";
        Assert.assertFalse(ValidatorEnvelopes.validData(argument));
    }

    @Test
    public void validDataInteger() throws Exception {
        argument = "4";
        Assert.assertFalse(ValidatorEnvelopes.validData(argument));
    }

    @Test
    public void validDataTwoDouble() throws Exception {
        argument = "4.5 3.0";
        Assert.assertFalse(ValidatorEnvelopes.validData(argument));
    }
}