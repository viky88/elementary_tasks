package com.company.tickets;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TicketsListTest {

    @Test(expected = IOException.class)
    public void readFileException() throws Exception {
        BufferedReader mock = mock(BufferedReader.class);
        doThrow(new IOException()).when(mock).close();
        mock.close();
    }

    @Test
    public void readFileClosesStream() throws Exception {
        BufferedReader mock = mock(BufferedReader.class);
        mock.close();
        verify(mock).close();
    }

    @Test
    public void readFileData() throws Exception {
        List<Ticket> actualList = new ArrayList<>();
        TicketsList ticketsList = new TicketsList(actualList);
        BufferedReader mock = mock(BufferedReader.class);

        when(mock.readLine()).thenReturn("333999").thenReturn("666777").thenReturn("111222").thenReturn("44332").
                thenReturn("333444").thenReturn(null);
        actualList = ticketsList.readTickets(mock, actualList);
        Object[] actual = actualList.toArray();

        List<Ticket> expectedList = new ArrayList<>();
        expectedList.add(new Ticket(333999));
        expectedList.add(new Ticket(666777));
        expectedList.add(new Ticket(111222));
        expectedList.add(new Ticket(333444));
        Object[] expected = expectedList.toArray();

        Assert.assertArrayEquals(expected, actual);
    }


    @Test
    public void countHappyTicketsMoscow() throws Exception {
        int expected = 3;
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(new Ticket(222233));
        tickets.add(new Ticket(123321));
        tickets.add(new Ticket(915753));
        tickets.add(new Ticket(654392));
        tickets.add(new Ticket(360453));
        tickets.add(new Ticket(445445));
        TicketsList ticketsList = new TicketsList(tickets);
        int actual = ticketsList.countLuckyTicketsMoscow(tickets);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void countHappyTicketsPiter() throws Exception {
        int expected = 3;
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(new Ticket(222233));
        tickets.add(new Ticket(123321));
        tickets.add(new Ticket(915753));
        tickets.add(new Ticket(654392));
        tickets.add(new Ticket(783597));
        tickets.add(new Ticket(445445));
        TicketsList ticketsList = new TicketsList(tickets);
        int actual = ticketsList.countLuckyTicketsPiter(tickets);

        Assert.assertEquals(expected, actual);
    }
}