package com.company.tickets;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class LuckyTicketsTest {

    @Test
    public void isLuckyMoskowTrue() throws Exception {
        Assert.assertTrue(LuckyTickets.isLuckyMoscow(123123));
    }

    @Test
    public void isLuckyMoskowFalse() throws Exception {
        Assert.assertFalse(LuckyTickets.isLuckyMoscow(123456));
    }


    @Test
    public void isLuckyPiterTrue() throws Exception {
        Assert.assertTrue(LuckyTickets.isLuckyPiter(198242));
    }

    @Test
    public void isLuckyPiterFalse() throws Exception {
        Assert.assertFalse(LuckyTickets.isLuckyPiter(191212));
    }


}