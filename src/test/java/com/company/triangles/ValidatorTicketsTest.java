package com.company.triangles;


import com.company.tickets.ValidatorTickets;
import org.junit.Assert;
import org.junit.Test;

public class ValidatorTicketsTest {

    @Test
    public void validatorTicketsMoscow() throws Exception {
        Assert.assertTrue(ValidatorTickets.validatorTickets("moscow"));
    }

    @Test
    public void validatorTicketsPiter() throws Exception {
        Assert.assertTrue(ValidatorTickets.validatorTickets("piter"));
    }

    @Test
    public void validatorTicketsEmpty() throws Exception {
        Assert.assertFalse(ValidatorTickets.validatorTickets(""));
    }

    @Test
    public void validatorTickets() throws Exception {
        Assert.assertFalse(ValidatorTickets.validatorTickets("something wrong"));
    }

    @Test
    public void validatorFileExistTrue() throws Exception {
        Assert.assertTrue(ValidatorTickets.validatorFileExist(
                "/Users/viktoriyasidenko/Documents/Projects/SoftServeITAcademy/elementary_tasks/src/main" +
                        "/tickets.txt"));
    }

    @Test
    public void validatorFileExistEmpty() throws Exception {
        Assert.assertFalse(ValidatorTickets.validatorFileExist(""));
    }

    @Test
    public void validatorFileExist() throws Exception {
        Assert.assertFalse(ValidatorTickets.validatorFileExist("/main.txt"));
    }

}
