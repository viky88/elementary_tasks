package com.company.triangles;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class TriangleTest {

    private final String[] paramForFirstTest = {"Triangle1", "3.0", "4.0", "3.0"};
    private final String[] paramForSecondTest = {"Triangle1", "5.0", "4.0", "6.0"};

    @Test
    public void getAreaEquals() throws Exception {
        double expected = 4.47213595499958;
        Triangle triangle = new Triangle(paramForFirstTest);
        double actual = triangle.getArea();

        Assert.assertEquals(expected, actual, 0.0);
    }

    @Test
    public void getAreaNotEquals() throws Exception {
        double unexpected = 7.47213595499958;
        Triangle triangle = new Triangle(paramForSecondTest);
        double actual = triangle.getArea();

        Assert.assertNotSame(unexpected, actual);
    }
}