package com.company.triangles;

import com.company.Validator;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class ValidatorTriangleTest {

    String param;

    @Test
    public void isTriangleTrue() throws Exception {
        Assert.assertTrue(ValidatorTriangle.isTriangle(3.0, 4.0, 3.0));
    }

    @Test
    public void isTriangleFalse() throws Exception {
        Assert.assertFalse(ValidatorTriangle.isTriangle(3.0, 20.0, 15.0));
    }

    @Test
    public void validDataTrue() throws Exception {
        param = "Triangle1, 3.0, 3.0, 3.0";
        Assert.assertTrue(ValidatorTriangle.validData(param));
    }

    @Test
    public void validDataEmpty() throws Exception {
        param = "";
        Assert.assertFalse(ValidatorTriangle.validData(param));
    }

    @Test
    public void validDataNot() throws Exception {
        param = "not, valid, data";
        Assert.assertFalse(ValidatorTriangle.validData(param));
    }

    @Test
    public void validDataNotDouble() throws Exception {
        param = "not, valid, data, and double";
        Assert.assertFalse(ValidatorTriangle.validData(param));
    }

    @Test
    public void validDataInteger() throws Exception {
        param = "Integer, 3, 7, 5";
        Assert.assertFalse(ValidatorTriangle.validData(param));
    }

    @Test
    public void isContinueTrueYLowCase() throws Exception {
        String param = "y";
        Assert.assertTrue(Validator.continueApp(param));
    }

    @Test
    public void isContinueTrueYUpperCase() throws Exception {
        String param = "Y";
        Assert.assertTrue(Validator.continueApp(param));
    }

    @Test
    public void isContinueTrueYes() throws Exception {
        String param = "yes";
        Assert.assertTrue(Validator.continueApp(param));
    }

    @Test
    public void isContinueFalse() throws Exception {
        String param = "word";
        Assert.assertFalse(Validator.continueApp(param));
    }

}