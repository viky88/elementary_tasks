package com.company.square;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class SquareOfNumberTest {

    SquareOfNumber squareOfNumber;
    List<Integer> expected = new ArrayList<>();
    List<Integer> actual;

    @Test
    public void getNumbersListEqualsEnterNum() throws Exception {
        squareOfNumber = new SquareOfNumber(25);
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        actual = squareOfNumber.getNumbersList();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getNumbersListNotEqualsEnterNum() throws Exception {
        squareOfNumber = new SquareOfNumber(28);
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        actual = squareOfNumber.getNumbersList();

        Assert.assertEquals(expected, actual);
    }

    @After
    public void clear() {
        expected.clear();
        actual.clear();
    }

}