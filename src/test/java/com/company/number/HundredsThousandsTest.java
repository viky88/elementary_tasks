package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class HundredsThousandsTest {

    private String expected;
    private String actual;

    @Test
    public void getHundredsThousandsByString100000() throws Exception {

        expected = "сто тысяч";
        actual = HundredsThousands.getHundredsThousandsByString(100000);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString101000() throws Exception {

        expected = "сто одна тысяча";
        actual = HundredsThousands.getHundredsThousandsByString(101000);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString111000() throws Exception {

        expected = "сто одиннадцать тысяч";
        actual = HundredsThousands.getHundredsThousandsByString(111000);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString123000() throws Exception {

        expected = "сто двадцать три тысячи";
        actual = HundredsThousands.getHundredsThousandsByString(123000);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString110000() throws Exception {

        expected = "сто десять тысяч";
        actual = HundredsThousands.getHundredsThousandsByString(110000);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString100001() throws Exception {

        expected = "сто тысяч один";
        actual = HundredsThousands.getHundredsThousandsByString(100001);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString101001() throws Exception {

        expected = "сто одна тысяча один";
        actual = HundredsThousands.getHundredsThousandsByString(101001);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString132006() throws Exception {

        expected = "сто тридцать две тысячи шесть";
        actual = HundredsThousands.getHundredsThousandsByString(132006);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString300016() throws Exception {

        expected = "триста тысяч шестнадцать";
        actual = HundredsThousands.getHundredsThousandsByString(300016);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString305066() throws Exception {

        expected = "триста пять тысяч шестьдесят шесть";
        actual = HundredsThousands.getHundredsThousandsByString(305066);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString361070() throws Exception {

        expected = "триста шестьдесят одна тысяча семьдесят";
        actual = HundredsThousands.getHundredsThousandsByString(361070);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString361479() throws Exception {

        expected = "триста шестьдесят одна тысяча четыресто семьдесят девять";
        actual = HundredsThousands.getHundredsThousandsByString(361479);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByString999999() throws Exception {

        expected = "девятьсот девяносто девять тысяч девятьсот девяносто девять";
        actual = HundredsThousands.getHundredsThousandsByString(999999);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void getHundredsThousandsByStringNotSame() throws Exception {

        expected = "девятьсот девяносто девять тысяч девятьсот девяносто девять";
        actual = HundredsThousands.getHundredsThousandsByString(100000);

        Assert.assertNotSame(expected, actual);

    }

}