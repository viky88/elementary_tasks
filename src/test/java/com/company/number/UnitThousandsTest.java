package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class UnitThousandsTest {

    private String expected;
    private String actual;

    @Test
    public void getUnitThousandsByString1000() throws Exception {

        expected = "одна тысяча";
        actual = UnitThousands.getUnitThousandsByString(1000);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString2001() throws Exception {

        expected = "две тысячи один";
        actual = UnitThousands.getUnitThousandsByString(2001);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString3019() throws Exception {

        expected = "три тысячи девятнадцать";
        actual = UnitThousands.getUnitThousandsByString(3019);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString4070() throws Exception {

        expected = "четыре тысячи семьдесят";
        actual = UnitThousands.getUnitThousandsByString(4070);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString5039() throws Exception {

        expected = "пять тысяч тридцать девять";
        actual = UnitThousands.getUnitThousandsByString(5039);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString7254() throws Exception {

        expected = "семь тысяч двести пятьдесят четыре";
        actual = UnitThousands.getUnitThousandsByString(7254);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByString9999() throws Exception {

        expected = "девять тысяч девятьсот девяносто девять";
        actual = UnitThousands.getUnitThousandsByString(9999);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitThousandsByStringNotSame() throws Exception {

        expected = "девять тысяч девятьсот девяносто девять";
        actual = UnitThousands.getUnitThousandsByString(5567);

        Assert.assertNotSame(expected, actual);
    }

}