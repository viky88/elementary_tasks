package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class DecadesThousandsTest {

    private String expected;
    private String actual;

    @Test
    public void getDecadesThousandsByString50000() throws Exception {

        expected = "пятьдесят тысяч";
        actual = DecadesThousands.getDecadesThousandsByString(50000);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString30003() throws Exception {

        expected = "тридцать тысяч три";
        actual = DecadesThousands.getDecadesThousandsByString(30003);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString31003() throws Exception {

        expected = "тридцать одна тысяча три";
        actual = DecadesThousands.getDecadesThousandsByString(31003);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString32023() throws Exception {

        expected = "тридцать две тысячи двадцать три";
        actual = DecadesThousands.getDecadesThousandsByString(32023);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString32020() throws Exception {

        expected = "тридцать две тысячи двадцать";
        actual = DecadesThousands.getDecadesThousandsByString(32020);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString30023() throws Exception {

        expected = "тридцать тысяч двадцать три";
        actual = DecadesThousands.getDecadesThousandsByString(30023);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString30020() throws Exception {

        expected = "тридцать тысяч двадцать";
        actual = DecadesThousands.getDecadesThousandsByString(30020);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString30100() throws Exception {

        expected = "тридцать тысяч сто";
        actual = DecadesThousands.getDecadesThousandsByString(30100);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString31108() throws Exception {

        expected = "тридцать одна тысяча сто восемь";
        actual = DecadesThousands.getDecadesThousandsByString(31108);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByString31178() throws Exception {

        expected = "тридцать одна тысяча сто семьдесят восемь";
        actual = DecadesThousands.getDecadesThousandsByString(31178);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesThousandsByStringNotSame() throws Exception {

        expected = "сто тысячь";
        actual = DecadesThousands.getDecadesThousandsByString(100000);

        Assert.assertNotSame(expected, actual);
    }

}