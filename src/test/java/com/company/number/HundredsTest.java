package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class HundredsTest {

    private String expected;
    private String actual;

    @Test
    public void getHundredsByString100() throws Exception {

        expected = "сто";
        actual = Hundreds.getHundredsByString(100);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHundredsByString101() throws Exception {

        expected = "сто один";
        actual = Hundreds.getHundredsByString(101);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHundredsByString115() throws Exception {

        expected = "сто пятнадцать";
        actual = Hundreds.getHundredsByString(115);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHundredsByString150() throws Exception {

        expected = "сто пятьдесят";
        actual = Hundreds.getHundredsByString(150);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHundredsByString199() throws Exception {

        expected = "сто девяносто девять";
        actual = Hundreds.getHundredsByString(199);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHundredsByStringNotSame() throws Exception {

        expected = "сто девяносто девять";
        actual = Hundreds.getHundredsByString(6);

        Assert.assertNotSame(expected, actual);
    }

}