package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class UnitsTest {

    private String expected;
    private String actual;

    @Test
    public void getUnitByString1() throws Exception {

        expected = "один";
        actual = Units.getUnitByString(1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitByString2() throws Exception {

        expected = "два";
        actual = Units.getUnitByString(2);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitByString9() throws Exception {

        expected = "девять";
        actual = Units.getUnitByString(9);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitByString0() throws Exception {

        expected = "ноль";
        actual = Units.getUnitByString(0);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getUnitByStringNotEquals() throws Exception {

        expected = "три";
        actual = Units.getUnitByString(2);

        Assert.assertNotSame(expected, actual);
    }

}