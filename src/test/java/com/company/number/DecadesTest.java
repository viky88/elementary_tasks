package com.company.number;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class DecadesTest {

    private String expected;
    private String actual;

    @Test
    public void getDecadesByString11() throws Exception {

        expected = "одиннадцать";
        actual = Decades.getDecadesByString(11);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesByString19() throws Exception {

        expected = "девятнадцать";
        actual = Decades.getDecadesByString(19);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesByString30() throws Exception {

        expected = "тридцать";
        actual = Decades.getDecadesByString(30);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesByString65() throws Exception {

        expected = "шестьдесят пять";
        actual = Decades.getDecadesByString(65);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getDecadesByString100() throws Exception {

        expected = "сто";
        actual = Decades.getDecadesByString(100);

        Assert.assertNotSame(expected, actual);
    }

}